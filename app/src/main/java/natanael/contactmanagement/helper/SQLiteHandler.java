package natanael.contactmanagement.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Address;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import natanael.contactmanagement.model.Contact;

public class SQLiteHandler extends SQLiteOpenHelper
{
    private static final String TAG = SQLiteHandler.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "contact_management";

    private static final String TABLE_CONTACT = "contacts";
    private static final String KEY_ID = "id";
    private static final String KEY_FIRSTNAME = "first_name";
    private static final String KEY_LASTNAME = "last_name";
    private static final String KEY_PROFILEPIC = "profile_pic";
    private static final String KEY_URL = "url";

    public SQLiteHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);

        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_CONTACT_TABLE = "CREATE TABLE " + TABLE_CONTACT + "("
                + KEY_ID + " TEXT PRIMARY KEY,"+ KEY_FIRSTNAME + " TEXT,"+ KEY_LASTNAME + " TEXT,"+ KEY_PROFILEPIC + " TEXT,"
                + KEY_URL + " TEXT" +")";
        db.execSQL(CREATE_CONTACT_TABLE);
    }

    public void updateContact(String id, String firstName, String lastName,String profilePic,String url)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, id);
        values.put(KEY_FIRSTNAME, firstName);
        values.put(KEY_LASTNAME, lastName);
        values.put(KEY_PROFILEPIC, profilePic);
        values.put(KEY_URL, url);

        db.update(TABLE_CONTACT, values, "id=?", new String[] {id});
        db.close();
    }

    public void addContact(Contact contact)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, contact.getFirstName());
        values.put(KEY_FIRSTNAME, contact.getFirstName());
        values.put(KEY_LASTNAME, contact.getLastName());
        values.put(KEY_PROFILEPIC, contact.getProfilePic());
        values.put(KEY_URL, contact.getUrl());

        // Inserting Row
        long id2 = db.insertWithOnConflict(TABLE_CONTACT, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        if (id2 == -1)
        {
            updateContact(contact.getId(),contact.getFirstName(),contact.getLastName(),contact.getProfilePic(),contact.getUrl());
        }

        db.close();
    }

    public void deleteContact()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_CONTACT, null, null);
        db.close();

        Log.d(TAG, "Deleted all contact info from sqlite");
    }

    public ArrayList<Contact> getContact()
    {
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACT;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0)
        {
            while (cursor.isAfterLast() == false)
            {
                Contact contact = new Contact();
                contact.setId(cursor.getString(0));
                contact.setFirstName(cursor.getString(1));
                contact.setLastName(cursor.getString(2));
                contact.setProfilePic(cursor.getString(3));
                contact.setUrl(cursor.getString(4));

                contacts.add(contact);

                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();

        return contacts;
    }
}