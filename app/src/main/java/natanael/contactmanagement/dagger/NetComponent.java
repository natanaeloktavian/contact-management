package natanael.contactmanagement.dagger;

import javax.inject.Singleton;

import dagger.Component;
import natanael.contactmanagement.MainActivity;
import natanael.contactmanagement.app.MyApp;

@Singleton
@Component(modules={AppModule.class, NetModule.class})
public interface NetComponent
{
    void inject(MyApp app);
    void inject(MainActivity activity);
}