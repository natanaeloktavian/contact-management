package natanael.contactmanagement.rest;

import java.util.ArrayList;
import java.util.List;

import natanael.contactmanagement.model.ContactDetail;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;
import natanael.contactmanagement.model.Contact;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface
{
    @GET("contacts.json")
    Call<ArrayList<Contact>> getContacts();

    @GET
    Call <ContactDetail> getContactDetail(@Url String url);

    @POST("contacts.json")
    Call <ContactDetail> pushContacts(@Body ContactDetail contactDetail);

    @PUT
    Call <ContactDetail> updateFavorite(@Url String url,@Body ContactDetail contactDetail);
}