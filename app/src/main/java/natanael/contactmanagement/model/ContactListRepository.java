package natanael.contactmanagement.model;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import natanael.contactmanagement.rest.ApiClient;
import natanael.contactmanagement.rest.ApiInterface;

public class ContactListRepository implements IContactListRepository
{
    @Inject Retrofit retrofit;

    @Override
    public void loadContacts(final OnFinishedListener listener)
    {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<Contact>> call = apiService.getContacts();
        call.enqueue(new Callback<ArrayList<Contact>>()
        {
            @Override
            public void onResponse(Call<ArrayList<Contact>>call, Response<ArrayList<Contact>> response)
            {
                ArrayList<Contact> contacts = response.body();
                listener.onFinished(contacts);
            }

            @Override
            public void onFailure(Call<ArrayList<Contact>>call, Throwable t)
            {
                listener.onFailure(t.getMessage());
            }
        });
    }
}