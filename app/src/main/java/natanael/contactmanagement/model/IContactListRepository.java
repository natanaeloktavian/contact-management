package natanael.contactmanagement.model;

import java.util.ArrayList;

public interface IContactListRepository
{
    interface OnFinishedListener
    {
        void onFinished(ArrayList<Contact> items);
        void onFailure(String message);
    }

    void loadContacts(OnFinishedListener listener);
}