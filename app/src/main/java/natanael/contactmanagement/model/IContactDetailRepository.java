package natanael.contactmanagement.model;

import java.util.ArrayList;

public interface IContactDetailRepository
{
    interface OnFinishedListener
    {
        void onFinished(ContactDetail item);
        void OnFavoriteChanged(ContactDetail item);
    }

    void loadContactDetail(OnFinishedListener listener,String url);

    void updateFavorite(OnFinishedListener listener,String url,ContactDetail contactDetail);
}