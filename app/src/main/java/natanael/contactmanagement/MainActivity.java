package natanael.contactmanagement;

import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import natanael.contactmanagement.helper.SQLiteHandler;
import natanael.contactmanagement.model.ContactComparator;
import rx.Observable;
import rx.android.view.OnClickEvent;
import rx.android.view.ViewObservable;
import rx.functions.Action1;
import natanael.contactmanagement.adapter.ContactAdapter;
import natanael.contactmanagement.app.MyApp;
import natanael.contactmanagement.model.Contact;
import natanael.contactmanagement.model.ContactListRepository;
import natanael.contactmanagement.model.RecyclerItemClickListener;
import natanael.contactmanagement.presenter.IContactListPresenter;
import natanael.contactmanagement.presenter.ContactListPresenter;
import natanael.contactmanagement.view.IContactListView;

public class MainActivity extends AppCompatActivity implements IContactListView
{
    private TextView emptyLabel;
    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private CoordinatorLayout coordinatorLayout;
    private IContactListPresenter presenter;
    private ContactAdapter contactAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SQLiteHandler db;
    private Boolean dontUpdateLocalData=false;
    static final int ADD_CONTACT_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new SQLiteHandler(getApplicationContext());
        coordinatorLayout = (CoordinatorLayout) this.findViewById(R.id.coordinatorLayout);
        recyclerView = (RecyclerView) this.findViewById(R.id.recyclerView);
        fab = (FloatingActionButton)this.findViewById(R.id.fab);
        emptyLabel = (TextView)this.findViewById(R.id.emptyLabel);

        presenter = new ContactListPresenter(this, new ContactListRepository());
        presenter.loadContacts();

        ((MyApp) getApplication()).getNetComponent().inject(this);

        Observable<OnClickEvent> clicksObservable = ViewObservable.clicks(fab);
        clicksObservable
                .subscribe(new Action1<OnClickEvent>()
                {
                    @Override
                    public void call(OnClickEvent onClickEvent)
                    {
                        navigateToAddContact();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contact_list, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == R.id.add)
        {
            navigateToAddContact();
        }

        return super.onOptionsItemSelected(item);
    }

    private void navigateToAddContact()
    {
        Intent intent = new Intent(getApplicationContext(),AddContactActivity.class);
        startActivityForResult(intent,ADD_CONTACT_REQUEST);
    }

    private void navigateToDetail(Contact contact)
    {
        Intent intent = new Intent(getApplicationContext(),ContactDetailActivity.class);
        intent.putExtra("id", contact.getId());
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==ADD_CONTACT_REQUEST)
        {
            if (resultCode == RESULT_OK)
            {
                presenter.loadContacts();
            }
        }
    }

    @Override
    protected void onDestroy()
    {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onItemClicked(Contact contact)
    {
        navigateToDetail(contact);
    }

    @Override
    public void onFailure(String message)
    {
        dontUpdateLocalData = true;
        ArrayList<Contact> contacts = db.getContact();
        Collections.sort(contacts, new ContactComparator());
        this.setItems(contacts);
        dontUpdateLocalData = false;

        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Not able to connect to Server, Loading from Local Database", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void setItems(ArrayList<Contact> contacts)
    {
        if(contacts==null || contacts.size()==0)
        {
            emptyLabel.setVisibility(View.VISIBLE);
        }
        else
        {
            emptyLabel.setVisibility(View.GONE);
            contactAdapter = new ContactAdapter(getApplicationContext(), contacts);
            if (recyclerView != null)
                this.initializeRecyclerView(recyclerView, contactAdapter);
            if(!dontUpdateLocalData)
            {
                for (Contact contact : contacts)
                {
                    db.addContact(contact);
                }
            }
        }
    }

    private void initializeRecyclerView(final RecyclerView recyclerView, final RecyclerView.Adapter adapter)
    {
        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this)
        {
            @Override
            public boolean canScrollVertically()
            {
                return  true;
            }
        };

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this.getApplicationContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener()
                {
                    @Override public void onItemClick(View view, int position)
                    {
                        presenter.onItemClicked(position);
                    }

                    @Override public void onLongItemClick(View view, int position)
                    {

                    }
                })
        );

        recyclerView.setAdapter(adapter);
    }
}
