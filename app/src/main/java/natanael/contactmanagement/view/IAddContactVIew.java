package natanael.contactmanagement.view;

import natanael.contactmanagement.model.ContactDetail;

public interface IAddContactVIew
{
    void onFinished(ContactDetail contactDetail);

    void onFailure(String message);
}