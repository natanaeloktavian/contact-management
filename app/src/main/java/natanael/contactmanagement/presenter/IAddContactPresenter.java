package natanael.contactmanagement.presenter;

public interface IAddContactPresenter
{
    void addContact(String firstName,String lastName,String email,String phoneNumber,Boolean favorite);

    void onDestroy();
}