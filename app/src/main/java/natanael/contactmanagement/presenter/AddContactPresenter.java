package natanael.contactmanagement.presenter;

import natanael.contactmanagement.model.ContactDetail;
import natanael.contactmanagement.model.IAddContactRepository;
import natanael.contactmanagement.model.IContactDetailRepository;
import natanael.contactmanagement.view.IAddContactVIew;
import natanael.contactmanagement.view.IContactDetailView;

public class AddContactPresenter implements IAddContactPresenter, IAddContactRepository.OnFinishedListener
{
    private IAddContactVIew mainView;
    private IAddContactRepository addContactRepository;

    public AddContactPresenter(IAddContactVIew mainView, IAddContactRepository findItemsInteractor)
    {
        this.mainView = mainView;
        this.addContactRepository = findItemsInteractor;
    }

    public IAddContactVIew getMainView()
    {
        return mainView;
    }

    @Override
    public void addContact(String firstName, String lastName, String email, String phoneNumber, Boolean favorite)
    {
        if(addContactRepository!=null)
            addContactRepository.addContact(this, firstName, lastName, email, phoneNumber, favorite);
    }

    @Override public void onDestroy()
    {
        mainView = null;
    }

    @Override
    public void onFinished(ContactDetail item)
    {
        if(mainView!=null)
            mainView.onFinished(item);
    }

    @Override
    public void onFailure(String message)
    {
        if(mainView!=null)
            mainView.onFailure(message);
    }
}