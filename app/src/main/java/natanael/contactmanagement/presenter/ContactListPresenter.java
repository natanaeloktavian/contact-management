package natanael.contactmanagement.presenter;

import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Collections;

import natanael.contactmanagement.model.Contact;
import natanael.contactmanagement.model.ContactComparator;
import natanael.contactmanagement.model.IContactListRepository;
import natanael.contactmanagement.view.IContactListView;

public class ContactListPresenter implements IContactListPresenter, IContactListRepository.OnFinishedListener
{
    private IContactListView mainView;
    private IContactListRepository findItemsInteractor;
    private ArrayList<Contact> items;

    public ContactListPresenter(IContactListView mainView,IContactListRepository findItemsInteractor)
    {
        this.mainView = mainView;
        this.findItemsInteractor = findItemsInteractor;
    }

    public IContactListView getMainView()
    {
        return mainView;
    }

    @Override
    public void loadContacts()
    {
        if(findItemsInteractor!=null)
            findItemsInteractor.loadContacts(this);
    }

    @Override
    public void onItemClicked(int position)
    {
        if (mainView != null && items!=null)
        {
            mainView.onItemClicked(items.get(position));
        }
    }

    @Override public void onDestroy()
    {
        mainView = null;
    }

    @Override
    public void onFinished(ArrayList<Contact> items)
    {
        Collections.sort(items, new ContactComparator());
        this.items = items;
        if(mainView!=null)
            mainView.setItems(items);
    }

    @Override
    public void onFailure(String message)
    {
        if(mainView!=null)
            mainView.onFailure(message);
    }
}