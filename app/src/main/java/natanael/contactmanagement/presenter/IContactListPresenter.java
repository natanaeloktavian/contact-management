package natanael.contactmanagement.presenter;

import natanael.contactmanagement.model.Contact;

public interface IContactListPresenter
{
    void loadContacts();

    void onItemClicked(int position);

    void onDestroy();
}