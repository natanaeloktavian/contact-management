package natanael.contactmanagement;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import natanael.contactmanagement.model.Contact;
import natanael.contactmanagement.model.IContactDetailRepository;
import natanael.contactmanagement.model.IContactListRepository;
import natanael.contactmanagement.presenter.ContactDetailPresenter;
import natanael.contactmanagement.presenter.ContactListPresenter;
import natanael.contactmanagement.view.IContactDetailView;
import natanael.contactmanagement.view.IContactListView;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ContactDetailPresenterTest
{
    @Mock
    IContactDetailView view;
    @Mock
    IContactDetailRepository interactor;

    private ContactDetailPresenter presenter;

    @Before
    public void setUp() throws Exception
    {
        presenter = new ContactDetailPresenter(view, interactor);
    }

    @Test
    public void checkIfViewIsReleasedOnDestroy()
    {
        presenter.onDestroy();
        assertNull(presenter.getMainView());
    }
}

