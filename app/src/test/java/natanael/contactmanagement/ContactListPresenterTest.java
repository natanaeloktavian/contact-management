package natanael.contactmanagement;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import natanael.contactmanagement.model.Contact;
import natanael.contactmanagement.model.IContactListRepository;
import natanael.contactmanagement.presenter.ContactListPresenter;
import natanael.contactmanagement.view.IContactListView;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ContactListPresenterTest
{
    @Mock
    IContactListView view;
    @Mock
    IContactListRepository interactor;

    private ContactListPresenter presenter;

    @Before
    public void setUp() throws Exception
    {
        presenter = new ContactListPresenter(view, interactor);
    }

    @Test
    public void checkIfShowsMessageOnFailure()
    {
        presenter.onFailure(anyString());
        verify(view, times(1)).onFailure(anyString());
    }

    @Test
    public void checkIfItemsArePassedToView()
    {
        Contact contact1 = new Contact();
        contact1.setFirstName("First Name 1");
        Contact contact2 = new Contact();
        contact2.setFirstName("First Name 2");

        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.add(contact1);
        contacts.add(contact2);

        presenter.onFinished(contacts);
        verify(view, times(1)).setItems(contacts);
    }


    @Test
    public void checkIfViewIsReleasedOnDestroy()
    {
        presenter.onDestroy();
        assertNull(presenter.getMainView());
    }
}

