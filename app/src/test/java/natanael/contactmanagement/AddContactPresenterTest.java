package natanael.contactmanagement;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import natanael.contactmanagement.model.AddContactRepository;
import natanael.contactmanagement.model.Contact;
import natanael.contactmanagement.model.IAddContactRepository;
import natanael.contactmanagement.model.IContactDetailRepository;
import natanael.contactmanagement.model.IContactListRepository;
import natanael.contactmanagement.presenter.AddContactPresenter;
import natanael.contactmanagement.presenter.ContactDetailPresenter;
import natanael.contactmanagement.presenter.ContactListPresenter;
import natanael.contactmanagement.view.IAddContactVIew;
import natanael.contactmanagement.view.IContactDetailView;
import natanael.contactmanagement.view.IContactListView;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AddContactPresenterTest
{
    @Mock
    IAddContactVIew view;
    @Mock
    IAddContactRepository interactor;

    private AddContactPresenter presenter;

    @Before
    public void setUp() throws Exception
    {
        presenter = new AddContactPresenter(view, interactor);
    }

    @Test
    public void checkIfShowsMessageOnFailure()
    {
        presenter.onFailure(anyString());
        verify(view, times(1)).onFailure(anyString());
    }

    @Test
    public void checkIfViewIsReleasedOnDestroy()
    {
        presenter.onDestroy();
        assertNull(presenter.getMainView());
    }
}

